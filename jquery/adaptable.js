jQuery(document).ready(function($) {

    //Bootstrap will close the alert because it spots the data-dismiss="alert" attribute
    //Here we also handle the alert close event. We have added two custom tags data-alertindex
    //and data-alertkey. e.g Alert1  has alertindex1. The alertkey value identifies the
    // alert content, since Alert1 (2 and 3) will be reused. We use a YUI function to set
    //the user preference for the key to the last dismissed key for the alertindex.
    //alertkey undismissable is a special case for "loginas" alert which shouldn't really
    //be permanently dismissed.
    //Justin 2015/12/05
   $('.close').click(function(){
      var alertindex = $(this).data('alertindex');
      var alertkey = $(this).data('alertkey');
      if(alertkey!='undismissable'){
         M.util.set_user_preference('theme_adaptable_alertkey' + alertindex, alertkey);
      }
    });

  $('#ticker').tickerme();
    //new for every three
    if($('header').css("position") == "fixed") {
        $('.outercont').css('padding-top', $('header').height());
    }
    var $pArr = $('.frontpage-course-list-all').children(".coursebox");
    var pArrLen = $pArr.length;
    var pPerDiv = 3;
    for (var i = 0;i < pArrLen;i+=pPerDiv){
        $pArr.filter(':eq('+i+'),:lt('+(i+pPerDiv)+'):gt('+i+')').wrapAll('<div class="row-fluid clearfix" />');
    }
    var $pArr = $('.frontpage-course-list-enrolled').children(".coursebox");
    var pArrLen = $pArr.length;
    var pPerDiv = 3;
    for (var i = 0;i < pArrLen;i+=pPerDiv){
            $pArr.filter(':eq('+i+'),:lt('+(i+pPerDiv)+'):gt('+i+')').wrapAll('<div class="row-fluid clearfix" />');
    }

// Breadcrumb ***************************************

    $(".breadcrumb li:not(:last-child) span").not('.separator').addClass('');
    $(".breadcrumb li a" );
    $(".breadcrumb li:last-child").addClass("lastli");


    var offset = 50;
    var duration = 500;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);
            if($('header').css("position") == "fixed") {
                jQuery('#page-header').hide();
                Y.Global.fire('moodle-gradereport_grader:resized');
            }
        } else {
            jQuery('.back-to-top').fadeOut(duration);
            if($('header').css("position") == "fixed") {
                jQuery('#page-header').show();
                Y.Global.fire('moodle-gradereport_grader:resized');
            }
        }
    });
    jQuery('.back-to-top').click(function(event) {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;
    })
});

// Zoom ************************************************
var onZoom = function() {
  var zoomin = Y.one('body').hasClass('zoomin');
  if (zoomin) {
    Y.one('body').removeClass('zoomin');
    M.util.set_user_preference('theme_adaptable_zoom', 'nozoom');
  } else {
    Y.one('body').addClass('zoomin');
    M.util.set_user_preference('theme_adaptable_zoom', 'zoomin');
  }
};

//When the button with class .moodlezoom is clicked fire the onZoom function
M.theme_adaptable = M.theme_adaptable || {};
M.theme_adaptable.zoom =  {
  init: function() {
    Y.one('body').delegate('click', onZoom, '.moodlezoom');
  }
};


var onFull = function() {
    if ( $("#page").hasClass("fullin") ) {
        $( "#page").removeClass('fullin');
        M.util.set_user_preference('theme_adaptable_full', 'nofull');
    } else {
        $( "#page").addClass('fullin');
        //Y.one('#page').addClass('fullin');
        M.util.set_user_preference('theme_adaptable_full', 'fullin');
    }
};

//When the button with class .moodlezoom is clicked fire the onZoom function
M.theme_adaptable = M.theme_adaptable || {};
M.theme_adaptable.full =  {
  init: function() {
    Y.one('body').delegate('click', onFull, '.moodlewidth');
  }
};
