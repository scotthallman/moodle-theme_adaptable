<!-- BRENDONS MENU STARTS HERE ---------------------->          
<div id="brnav-container">

  <!-- HELP Menu -->  
  <div id="helpmenu" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">Help Navigation</h2></li>
            <li class="home">
	      <a title="Contact Support- email med.moodle@otago.ac.nz" href="mailto:med.moodle@otago.ac.nz?Subject=Help with Moodle&body=In order to help you, we need details of your browser and operating system. %0D%0DPLEASE DO THE FOLLOWING%0D%0D1. go to http://www.otago.ac.nz/checkinfo %0D%0D2. COPY the information from the first white box. %0D%0D3. paste the results into this email. %0D%0DNow please describe the issue you are experiencing:">Contact Support- email med.moodle@otago.ac.nz</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#helpmenu">Close X</button>
	    </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  
          <ul class="unstyled span3 mylist">
            <li><a title="Moodle Student Support" href="https://medschool.otago.ac.nz/course/view.php?id=214">Moodle Student Support</a></li>
            
          </ul> 
          <ul class="unstyled span3 mylist"> 
            <li><a title="Moodle Staff Support" href="https://medschool.otago.ac.nz/course/view.php?id=143">Moodle Staff Support</a></li>
            
          </ul>
          <ul class="unstyled span3 mylist">

            <li><a title="Video Tutorials for staff" href="https://medschool.otago.ac.nz/course/view.php?id=143#section-2">Video Tutorials for staff</a></li>
          </ul>
          <ul class="unstyled span3 mylist">

            <li><a title="Moodle Handbook" href="http://facmed.otago.ac.nz/elearning/wp-content/uploads/sites/6/2010/12/using_moodle_2e.pdf">Moodle Handbook for staff</a>

          </ul>

        </div>
      </li>
      
    </ul>  
  </div>


  <!-- STAFF Menu -->  
  <div id="staffmenu" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">Staff Support Navigation</h2></li>
            <li class="home">
	      <a title="Staff Teaching and Learning Support" href="https://medschool.otago.ac.nz/course/view.php?id=224">Staff Teaching and Learning Support</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#staffmenu">Close X</button>
	    </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  

          <ul class="unstyled span4 mylist">
            <li class="programme">Moodle Resources</li>
            <li><a title="Moodle Staff Support" href="https://medschool.otago.ac.nz/course/view.php?id=143">Moodle Staff Support</a></li>
            <li><a title="Moodle Handbook" href="http://facmed.otago.ac.nz/elearning/wp-content/uploads/sites/6/2010/12/using_moodle_2e.pdf">Moodle Handbook for staff</a></li>
            <li><a title="Video Tutorials for staff" href="https://medschool.otago.ac.nz/course/view.php?id=143#section-2">Video Tutorials for staff</a></li>
          </ul> 

          <ul class="unstyled span4 mylist"> 
            <li class="programme">eLearning Resources</li>
            <li><a title="Examples Course" href="https://medschool.otago.ac.nz/course/view.php?id=479">Examples Course</a></li>
            <li><a title="How To" href="http://elearning.healthsci.otago.ac.nz">How To</a></li>
          </ul>

          <ul class="unstyled span4 mylist">
            <li class="programme">Support</li>
            <li>
	      <a title="Contact Support- email med.moodle@otago.ac.nz" href="mailto:med.moodle@otago.ac.nz?Subject=Help with Moodle&body=In order to help you, we need details of your browser and operating system. %0D%0DPLEASE DO THE FOLLOWING%0D%0D1. go to http://www.otago.ac.nz/checkinfo %0D%0D2. COPY the information from the first white box. %0D%0D3. paste the results into this email. %0D%0DNow please describe the issue you are experiencing:">Contact Support- email med.moodle@otago.ac.nz</a>
	    </li>
            <li><a title="PostGrad" href="https://hsmoodle.otago.ac.nz">PostGrad</a></li>
          </ul>

        </div>
      </li>
      
    </ul>  
  </div>


  <!-- ELM 2 Menu -->  
  <div id="elm2" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ELM 2 Navigation</h2></li>
            <li class="home"><a title="ELM 2 Home" href="https://medschool.otago.ac.nz/course/view.php?id=1258">ELM 2 Home</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#elm2">Close X</button>
	  		</li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  

          <ul class="unstyled span3 mylist">
            <!-- elm 2 BLOCK MODULES -->        
            <li class="programme"><div>Block Modules</div></li>
            <li><a title="Psychological Medicine (Block)" href="https://medschool.otago.ac.nz/course/view.php?id=1274">Psychological Medicine (Block)</a></li>
            <li><a title="Cardiovascular System" href="https://medschool.otago.ac.nz/course/view.php?id=1259">Cardiovascular System</a></li>
            <li><a title="Gastrointestinal System" href="https://medschool.otago.ac.nz/course/view.php?id=1260">Gastrointestinal System</a></li>
            <li><a title="Musculoskeletal System" href="https://medschool.otago.ac.nz/course/view.php?id=1261">Musculoskeletal System</a></li>
            <li><a title="Respiratory System" href="https://medschool.otago.ac.nz/course/view.php?id=1277">Respiratory System</a></li>
          </ul> 

          <ul class="unstyled span3 mylist">
            <!-- elm 2 VERTICAL MODULES --> 
            <li class="programme">Vertical Modules</li>   
            <li><a title="Blood" href="https://medschool.otago.ac.nz/course/view.php?id=1256">Blood</a></li>
            <li><a title="Cancer" href="https://medschool.otago.ac.nz/course/view.php?id=1257">Cancer</a></li>
            <li><a title="EBP" href="https://medschool.otago.ac.nz/course/view.php?id=1263">EBP</a></li>
            <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1264">Palliative Medicine and End of Life Care</a></li>
            <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1266">Ethics</a></li>
            <li><a title="Genetics" href="https://medschool.otago.ac.nz/course/view.php?id=1267">Genetics</a></li>
            <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1268">Hauora Māori</a></li>
            <li><a title="Infection &amp; Immunology" href="https://medschool.otago.ac.nz/course/view.php?id=1269">Infection &amp; Immunology</a></li>
            <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1270">Pathology</a></li>
            <li><a title="Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1271">Pharmacology</a></li>
            <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1272">Professional Development</a></li>
            <li><a title="Psychological Medicine (Vertical)" href="https://medschool.otago.ac.nz/course/view.php?id=1273">Psychological Medicine (Vertical)</a></li>
            <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1275">Public Health</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 2 PROGRAMME MODULES -->
            <li class="programme">Programmes</li>
            <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1278">Clinical Skills</a></li>
            <li><a title="Early Professional Experience" href="https://medschool.otago.ac.nz/course/view.php?id=1265">Early Professional Experience</a></li>
            <li><a title="Integrated Cases" href="https://medschool.otago.ac.nz/course/view.php?id=1262">Integrated Cases</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 2 PROGRAMME MODULES -->
            <li class="programme">Other</li>
            <li><a title="2016 HSFY" href="https://medschool.otago.ac.nz/course/view.php?id=1286">2016 HSFY</a></li>
            <li><a title="2017 ResearchSmart" href="https://medschool.otago.ac.nz/course/view.php?id=1276">2017 ResearchSmart</a></li>
          </ul>
        </div>
      </li>
      
    </ul>  
  </div>


  <div id="elm3" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ELM 3 Navigation</h2></li>
            <li class="home"><a title="ELM 3 Home" href="https://medschool.otago.ac.nz/course/view.php?id=1066">ELM 3 Home</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#elm3">Close X</button>
		    </li>
	        <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  

          <ul class="unstyled span3 mylist">
            <!-- elm 3 BLOCK MODULES -->        
            <li class="programme"><div>Block Modules</div></li>
            <li><a title="Psychological Medicine (block)" href="https://medschool.otago.ac.nz/course/view.php?id=911">Psychological Medicine (block)</a></li>
            <li><a title="Cardiovascular System" href="https://medschool.otago.ac.nz/course/view.php?id=1071">Cardiovascular System</a></li>
            <li><a title="Endocrine" href="https://medschool.otago.ac.nz/course/view.php?id=1279"> Endocrine</a></li>
            <li><a title="GastroIntestinal System" href="https://medschool.otago.ac.nz/course/view.php?id=1082">GastroIntestinal System</a></li>
            <li><a title="Metabolism Nutrition " href="https://medschool.otago.ac.nz/course/view.php?id=1280"> Metabolism Nutrition</a></li>

            <li><a title="Musculoskeletal System" href="https://medschool.otago.ac.nz/course/view.php?id=1083">Musculoskeletal System</a></li>
            <li><a title="Nervous System " href="https://medschool.otago.ac.nz/course/view.php?id=1281"> Nervous System</a></li>
            <li><a title="RCA" href="https://medschool.otago.ac.nz/course/view.php?id=1282"> RCA</a></li>
            <li><a title="RDA" href="https://medschool.otago.ac.nz/course/view.php?id=1283"> RDA</a></li>
            <li><a title="Renal" href="https://medschool.otago.ac.nz/course/view.php?id=1284"> Renal</a></li>

            <li><a title="Respiratory System" href="https://medschool.otago.ac.nz/course/view.php?id=1084">Respiratory System</a></li>
          </ul> 

          <ul class="unstyled span3 mylist">
            <!-- elm 3 VERTICAL MODULES --> 
            <li class="programme">Vertical Modules</li>   
            <li><a title="Blood" href="https://medschool.otago.ac.nz/course/view.php?id=1072">Blood</a></li>
            <li><a title="Cancer" href="https://medschool.otago.ac.nz/course/view.php?id=1073">Cancer</a></li>
            <li><a title="EBP" href="https://medschool.otago.ac.nz/course/view.php?id=1074">EBP</a></li>
            <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1070">Ethics</a></li>
            <li><a title="Genetics" href="https://medschool.otago.ac.nz/course/view.php?id=1076">Genetics</a></li>
            <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1077">Hauora Māori</a></li>
            <li><a title="Infection &amp; Immunology" href="https://medschool.otago.ac.nz/course/view.php?id=1078">Infection &amp; Immunology</a></li>
            <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1075">Palliative Medicine and End of Life Care</a></li>
            <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1079">Pathology</a></li>
            <li><a title="Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1080">Pharmacology</a></li>
            <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1081">Professional Development</a></li>
            <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1096">Psychological Medicine</a></li>
            <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1069">Public Health</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 3 PROGRAMME MODULES -->
            <li class="programme">Programmes</li>
            <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1091">Clinical Skills</a></li>
            <li><a title="Early Professional Experience" href="https://medschool.otago.ac.nz/course/view.php?id=1093">Early Professional Experience</a></li>
            <li><a title="Integrated Cases" href="https://medschool.otago.ac.nz/course/view.php?id=1092">Integrated Cases</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 3 PROGRAMME MODULES -->
            <li class="programme">Other</li>
            <li><a title="2014 HSFY" href="https://medschool.otago.ac.nz/course/view.php?id=1095">2015 HSFY</a></li>
            <li><a title="2015 Studysmart" href="https://medschool.otago.ac.nz/course/view.php?id=958">2016 Researchsmart</a></li>
          </ul>
        </div>
      </li>
      
    </ul>  
  </div>


  <!-- ALM4 Menu -->
  <div id="alm4" class="collapse brnav-block">
    <ul class="container-fluid mymenu">
      <li>
        <div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ALM 4 Navigation</h2></li>
            <li class="home"><a title="ALM 4 - Home" href="https://medschool.otago.ac.nz/course/view.php?id=1153">ALM 4 - Home</a><button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#alm4">Close X</button>
	    </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">

          <ul class="unstyled span4 mylist">
            <!--UOC-->
            <li class="school-title">UOC</li>

            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Addiction Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1348">Addiction Medicine</a></li>   
              <li><a title="CardioRespiratory, Vascular, Plastic Surgery, Dermatology" href="https://medschool.otago.ac.nz/course/view.php?id=1349">CardioRespiratory, Vascular, Plastic Surgery, Dermatology</a></li>
              <li><a title="General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1350">General Practice</a></li>
              <li><a title="Healthcare of the Elderly" href="https://medschool.otago.ac.nz/course/view.php?id=1351">Healthcare of the Elderly</a></li>
              <li><a title="Surgery / Emergency Medicine / Gastroenterology / Oncology" href="https://medschool.otago.ac.nz/course/view.php?id=1352">Surgery / Emergency Medicine / Gastroenterology / Oncology</a></li>
              <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1354">Public Health</a></li>    

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1355">Clinical Skills</a></li>
              <li><a title="Ethics &amp; Law" href="https://medschool.otago.ac.nz/course/view.php?id=1356">Ethics &amp; Law</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1329">Hauora Māori</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1357">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1446">Pathology</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1358">Professional Development</a></li>
              <li><a title="Quality and Safety" href="https://medschool.otago.ac.nz/course/view.php?id=1359">Quality and Safety</a></li>

              <li><br /></li>

              <li class="programme">Other</li>
              <li><a title="Clinical Orientation 2018" href="https://medschool.otago.ac.nz/course/view.php?id=1385">Clinical Orientation 2018</a></li>
            </ul>
	    
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--DSM-->
            <li class="school-title">DSM</li>
            
            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Medicine 1" href="https://medschool.otago.ac.nz/course/view.php?id=1162">Medicine 1</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1168">Psychological Medicine</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1171">Surgery</a></li>
              <li><a title="Urban GP / ENT / Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1160">Urban GP / ENT / Public Health</a></li>

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Communication Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1157">Communication Skills</a></li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1165">Clinical Pharmacology</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1170">Clinical Skills</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1159">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1156">Ethics</a></li>
              <li><a title="Evidence Based Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1158">Evidence Based Practice</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1161">Hauora Māori</a></li>
              <li><a title="Microbiology" href="https://medschool.otago.ac.nz/course/view.php?id=1163">Microbiology</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1164">Pathology</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1167">Professional Development</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1169">Radiology</a></li>

              <li><br /></li>

              <li class="programme">Other</li>
              <li><a title="Whole Class Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=1172">Whole Class Teaching</a></li>
              <li><a title="Pregnancy Long Case" href="https://medschool.otago.ac.nz/course/view.php?id=1166">Pregnancy Long Case</a></li>        

            </ul>

          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--UOW-->
            <li class="school-title">UOW</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Advanced Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1195">Advanced Clinical Skills</a></li>
              <li><a title="General Practice and Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1197">General Practice and Public Health</a></li>
              <li><a title="Medicine and Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1199">Medicine and Clinical Skills</a></li>
              <li><a title="Surgical and Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1202">Surgery and Clinical Skills</a></li>

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Decision Making" href="https://medschool.otago.ac.nz/course/view.php?id=1196">Clinical Decision Making</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1198">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1200">Pathology</a></li>
              <li><a title="Professional Skills Attitudes and Ethics (PSAE)" href="https://medschool.otago.ac.nz/course/view.php?id=1201">Professional Skills Attitudes and Ethics (PSAE)</a></li>

            </ul>

          </ul>
          
          <!-- Normal Previous Years-->
          <!--<div class="row-fluid">-->
          <ul class="unstyled span4 mylist">
            <li class="school-title">Previous Years</li>
            <li class="">
              <a href="https://medschool.otago.ac.nz/course/index.php?categoryid=128" title="Previous Years">2015-2016 ELM</a>
            </li>
          </ul>
	</div>
      </li>
    </ul>
  </div>


  <!-- ALM5 Menu -->
  <div id="alm5" class="collapse brnav-block">
    
    <!-- ALM 5 Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
	<div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ALM 5 Navigation</h2></li>
            <li class="home">
	      <a title="ALM 5 - Home" href="https://medschool.otago.ac.nz/course/view.php?id=1154">ALM 5 - Home</a>
	      <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#alm5">Close X</button>
	    </li>
	    <li class="home_divider"></li>
          </ul>
	</div>
	<div class="row-fluid">
          <ul class="unstyled span4 mylist">
            <!--UOC-->
            <li class="school-title">UOC</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Advanced Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1360">Advanced Medicine</a></li>
              <li><a title="Orthopaedics and Advanced Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1361">Orthopaedics and Advanced Surgery</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1362">Paediatrics</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1363">Psychological Medicine</a></li>
              <li><a title="Womens Health and Developmental Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1364">Womens Health and Developmental Medicine</a></li>  
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
              <li><a title="Addiction Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1365">Addiction Medicine</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1366">Clinical Skills</a></li>
              <li><a title="Ethics & Law" href="https://medschool.otago.ac.nz/course/view.php?id=1367">Ethics & Law</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1342">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1447">Pathology</a></li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1368">Clinical Pharmacology</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1369">Professional Development</a></li>
              <li><a title="Quality and Safety" href="https://medschool.otago.ac.nz/course/view.php?id=1370">Quality and Safety</a></li>
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--DSM-->
            <li class="school-title">DSM</li>
            
            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Child Health & Reproductive Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1173">Child Health & Reproductive Medicine</a></li>
              <li><a title="Medicine 2" href="https://medschool.otago.ac.nz/course/view.php?id=1177">Medicine 2</a></li>
              <li><a title="Musculoskeletal, Anaesthesia and Intensive Care" href="https://medschool.otago.ac.nz/course/view.php?id=1179">Musculoskeletal, Anaesthesia and Intensive Care</a></li>
              <li><a title="Rural GP" href="https://medschool.otago.ac.nz/course/view.php?id=1182">Rural GP</a></li>
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1165">Clinical Pharmacology</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1170">Clinical Skills</a></li>
              <li><a title="End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1159">End of Life Care</a></li>
              <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1156">Ethics</a></li>
              <li><a title="Evidence Based Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1158">Evidence Based Practice</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1176">Hauora Māori</a></li>
              <li><a title="Microbiology" href="https://medschool.otago.ac.nz/course/view.php?id=1163">Microbiology</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1164">Pathology</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1181">Professional Development</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1169">Radiology</a></li>
              
              <li><br /></li>

              <li class="programme">Other</li>
              <li><a title="Whole Class Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=1183">Whole Class Teaching</a></li>
              <li><a title="Paediatric Chronic Longitudinal Case" href="https://medschool.otago.ac.nz/course/view.php?id=1100">Paediatric Chronic Longitudinal Case</a></li>                       
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--UOW-->
            <li class="school-title">UOW</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Primary Health Care and General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1206">Primary Health Care and General Practice</a></li>
              <li><a title="Musculoskeletal and Skin" href="https://medschool.otago.ac.nz/course/view.php?id=1209">Musculoskeletal and Skin</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1214">Psychological Medicine</a></li>
              <li><a title="General Medicine and Subspecialities" href="https://medschool.otago.ac.nz/course/view.php?id=1205">General Medicine and Subspecialities</a></li>
              <li><a title="Child and Adolescent Health" href="https://medschool.otago.ac.nz/course/view.php?id=1210">Child and Adolescent Health</a></li>
              <li><a title="Reproductive, Sexual and Women’s Health" href="https://medschool.otago.ac.nz/course/view.php?id=1216">Reproductive, Sexual and Women’s Health</a></li>                        
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1212">Clinical Pharmacology</a></li>
              <li><a title="Clinical Decision Making" href="https://medschool.otago.ac.nz/course/view.php?id=1203">Clinical Decision Making</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1207">Hauora Māori</a></li>
              <li><a title="Medical Imaging" href="https://medschool.otago.ac.nz/course/view.php?id=1208">Medical Imaging</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1211">Pathology</a></li>
              <li><a title="Professional Skills Attitudes and Ethics (PSAE)" href="https://medschool.otago.ac.nz/course/view.php?id=1213">Professional Skills Attitudes and Ethics (PSAE)</a></li>
              <li><a title="Palliative Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1215">Palliative Medicine</a></li>

            </ul>
          </ul>
	</div>
	<div class="row-fluid">
          
          <ul class="unstyled span4 mylist">
            <li class="school-title">RMIP</li>
            <li><a title="Hauora Māori for RMIP" href="https://medschool.otago.ac.nz/course/view.php?id=1293">Hauora Māori for RMIP</a></li>
            <li><a title="RMIP Home" href="https://medschool.otago.ac.nz/course/view.php?id=1291">RMIP Home</a></li>
          </ul>
          
          <!-- Normal Previous Years-->
          <!--<div class="row-fluid">-->
          <ul class="unstyled span4 mylist">
            <li class="school-title">Previous Years</li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=123" title="Previous Years">2014-2015 ELM</a></li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=129" title="Previous Years">2016 ALM 4</a></li>
          </ul>
	</div>	
      </li>
    </ul>  
  </div>


  <!-- ALM6 Menu -->
  <div id="alm6" class="collapse brnav-block">
    <ul class="container-fluid mymenu">
      <li>
	<div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ALM 6 Navigation</h2></li>
            <li class="home">
	      <a title="ALM 6 - Home" href="https://medschool.otago.ac.nz/course/view.php?id=1613">ALM 6 - Home</a>
	      <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#alm6">Close X</button>
	    </li>
            <li class="home_divider"></li>
          </ul>
	</div>
	<div class="row-fluid">
          <ul class="unstyled span4 mylist">
            <!--UOC-->
            <li class="school-title">UOC</li>
            <ul class="unstyled">

              <li class="programme">Block Modules</li>
              <li><a title="Elective" href="https://medschool.otago.ac.nz/course/view.php?id=1541">Elective</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1542">Paediatrics</a></li>
              <li><a title="Obstetrics & Gynaecology" href="https://medschool.otago.ac.nz/course/view.php?id=1544">Obstetrics &amp; Gynaecology</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1545">Psychological Medicine</a></li>
              <li><a title="Critical Care" href="https://medschool.otago.ac.nz/course/view.php?id=1546">Critical Care</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1547">Surgery</a></li>
              <li><a title="Selective" href="https://medschool.otago.ac.nz/course/view.php?id=1548">Selective</a></li>
              <li><a title="General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1549">General Practice</a></li>
              <li><a title="Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1550">Medicine</a></li>
	      
              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1530">Hauora Māori</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1551">Professional Development</a></li>
              <li><a title="Friday Afternoon Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=1552">Friday Afternoon Teaching</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1542">Clinical Skills</a></li>
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--DSM-->
            <li class="school-title">DSM</li>
            
            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Elective" href="https://medschool.otago.ac.nz/course/view.php?id=1186">Elective</a></li>
              <li><a title="Obstetrics & Gynaecology" href="https://medschool.otago.ac.nz/course/view.php?id=1188">Obstetrics & Gynaecology</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1189">Paediatrics</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1192">Psychological Medicine</a></li>
              <li><a title="Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1187">Medicine</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1193">Surgery</a></li>
              <li><a title="Community – Evaluation – Outpatients" href="https://medschool.otago.ac.nz/course/view.php?id=1184">Community – Evaluation – Outpatients</a></li>
              <li><a title="Critical Care and Emergency Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1185">Critical Care and Emergency Medicine</a></li>
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1165">Clinical Pharmacology</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1191">Professional Development</a></li>
              
              <li><br /></li>

              <li class="programme">Other</li>
              <!--<li><a title="2015d5 Whole Class Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=771">Whole Class Teaching</a></li>-->
              
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--UOW-->
            <li class="school-title">UOW</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Elective" href="https://medschool.otago.ac.nz/course/view.php?id=1141">Elective</a></li>
              <li><a title="Primary Health Care & General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1142">Primary Health Care & General Practice</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1143">Paediatrics</a></li>
              <li><a title="Obstetrics & Gynaecology" href="https://medschool.otago.ac.nz/course/view.php?id=1144">Obstetrics &amp; Gynaecology</a></li>
              <li><a title="Emergency Medicine / Acute Care" href="https://medschool.otago.ac.nz/course/view.php?id=1138">Emergency Medicine / Acute Care</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1137">Surgery</a></li>
              <li><a title="Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1145">Medicine</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1146">Psychological Medicine</a></li>

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Professional Skills, Attitudes & Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1147">Professional Skills, Attitudes & Ethics</a></li>

              <li><br /></li>        

              <!--<li class="programme">Other</li>-->
            </ul>
          </ul>
	</div>
	<div class="row-fluid">        
          <!-- Normal Previous Years-->
          <!--<div class="row-fluid">-->
          <ul class="unstyled span4 mylist">
            <li class="school-title-sm">Cross-campus</li>
            <li><a title="TIs as Teachers by Distance Learning" href="https://medschool.otago.ac.nz/course/view.php?id=1194">TIs as Teachers by Distance Learning</a></li>
          </ul>
          <ul class="unstyled span4 mylist">
            <li class="school-title">Previous Years</li>
            <li><a title="2012-2015 Content" href="https://medschool.otago.ac.nz/course/index.php?categoryid=57">2013-2016 Content</a></li>
          </ul>
	</div>          
	
      </li>
    </ul>  
  </div>
