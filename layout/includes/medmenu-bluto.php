<!-- BRENDONS MENU STARTS HERE ---------------------->          
<div id="brnav-container">
  
  <!--
      ********************************************************************************
      *     NURS Menu 
      ********************************************************************************
    -->
  <div id="nurs" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
    <div class="row-fluid" >
      <ul class="unstyled span12">
        <li><h2 class="brnav-heading">PGNS Navigation<h2></li>
        <li class="home">&nbsp;
          <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#nurs">Close X</button>
        </li>
        <li class="home_divider"></li>
      </ul>
    </div>
    <div class="row-fluid">  
      <ul class="unstyled span3 mylist">
        
        <!-- PGNS SEMESTER 1 -->        
        <li class="programme"><div>Semester 1</div></li>
        <li><a title="Nurs405 - Health Assessment and Advanced Nursing Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=507">NURS405 - Health Assessment and Advanced Nursing Practice</a></li>
        <li><a title="NURS415 - Nursing Research Methods" href="https://hsmoodle.otago.ac.nz/course/view.php?id=508">NURS415 - Nursing Research Methods</a></li>
        <li><a title="NURS423 - Nursing - Leadership and Management 1" href="https://hsmoodle.otago.ac.nz/course/view.php?id=509">NURS423 - Nursing - Leadership and Management 1</a></li>
        <li><a title="NURS427 - Long-term Condition Management" href="https://hsmoodle.otago.ac.nz/course/view.php?id=490">NURS427 - Long-term Condition Management</a></li>
        <li><a title="NURS429 - Therapeutics for Advanced Nursing Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=510">NURS429 - Therapeutics for Advanced Nursing Practice</a></li>        
        <li><br /></li>
        
        <li class="programme">Masters</li>
        <li><a title="2017 Masters Research School" href="https://hsmoodle.otago.ac.nz/course/view.php?id=500">2018 Masters Research School</a></li>
      </ul>
      <ul class="unstyled span3 mylist">
        <!-- PGNS SEMESTER 2 --> 
        <li class="programme">Semester 2</li>   
        <li><a title="NURS4 - Bioscience for Pharmacology and Clinical Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=511">NURS4 - Bioscience for Pharmacology and Clinical Practice</a></li>
        <li><a title="NURS411 - Nursing - High Acuity" href="https://hsmoodle.otago.ac.nz/course/view.php?id=521">NURS411 - Nursing - High Acuity</a></li>
        <li><a title="NURS413 - Primary Health Care Nursing - Rural/Urban" href="https://hsmoodle.otago.ac.nz/course/view.php?id=520">NURS413 - Primary Health Care Nursing - Rural/Urban</a></li>
        <li><a title="NURS416 - Nursing - Applied Pharmacology" href="https://hsmoodle.otago.ac.nz/course/view.php?id=519">NURS416 - Nursing - Applied Pharmacology</a></li>
        <li><a title="NURS418 - Nursing Education - Principles and Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=518">NURS418 - Nursing Education - Principles and Practice</a></li>
        <li><a title="NURS419 - Special Topic: Ageing and Mental Health" href="https://hsmoodle.otago.ac.nz/course/view.php?id=517">NURS419 - Special Topic: Ageing and Mental Health</a></li>
        <li><a title="NURS424 - Nursing - Leadership and Management 2" href="https://hsmoodle.otago.ac.nz/course/view.php?id=516">NURS424 - Nursing - Leadership and Management 2</a></li>
        <li><a title="NURS426 - Nursing - Gerontology" href="https://hsmoodle.otago.ac.nz/course/view.php?id=515">NURS426 - Nursing - Gerontology</a></li>
        <li><a title="NURS428 - Long-term Condition Management (Advanced)" href="https://hsmoodle.otago.ac.nz/course/view.php?id=514">NURS428 - Long-term Condition Management (Advanced)</a></li>
        <li><a title="NURS430 - Advanced Nursing Practicum" href="https://hsmoodle.otago.ac.nz/course/view.php?id=512">NURS430 - Advanced Nursing Practicum</a></li>
        <li><a title="NURS431 - Rural Nursing" href="https://hsmoodle.otago.ac.nz/course/view.php?id=513">NURS431 - Rural Nursing</a></li>
      </ul>
      <ul class="unstyled span3 mylist">
        <!-- 2017 Master of Nursing Science Degree -->
        <li class="programme">2017 Master of Nursing Science Degree Yr2</li>
        <li><a title="Master of Nursing Science 2017" href="https://hsmoodle.otago.ac.nz/course/view.php?id=419">Master of Nursing Science 2017</a></li>
        <li><a title="NURS 501 - Nursing Science 3" href="https://hsmoodle.otago.ac.nz/course/view.php?id=428">NURS501 - Nursing Science 3</a></li>
        <li><a title="NURS 502 - Mental Health Nursing" href="https://hsmoodle.otago.ac.nz/course/view.php?id=429">NURS502 - Mental Health Nursing</a></li>
        <li><a title="NURS 503 - Complex Health States" href="https://hsmoodle.otago.ac.nz/course/view.php?id=430">NURS503 - Complex Health States</a></li>
        <li><a title="NURS 504 - Consolidation of Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=431">NURS504 - Consolidation of Practice</a></li>
        <li><a title="NURS 580 - Nursing Research Project" href="https://hsmoodle.otago.ac.nz/course/view.php?id=432">NURS580 - Nursing Research Project</a></li>
        <li><a title="NURS 581 - Integrating Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=433">NURS581 - Integrating Practice</a></li>
        
        <li><br /></li>
        
        <li class="programme">2017 Master of Nursing Science Degree Yr1</li>
        <li><a title="Nursing Science Pre-Course" href="https://hsmoodle.otago.ac.nz/course/view.php?id=427">Nursing Science Pre-Course</a></li>
        <li><a title="NURS 441 - Professional Nursing" href="https://hsmoodle.otago.ac.nz/course/view.php?id=420">NURS441 - Professional Nursing</a></li>
        <li><a title="NURS 442 - Health Care in NZ" href="https://hsmoodle.otago.ac.nz/course/view.php?id=421">NURS442 - Health Care in NZ</a></li>
        <li><a title="NURS 443 - Nursing Science 1" href="https://hsmoodle.otago.ac.nz/course/view.php?id=422">NURS443 - Nursing Science 1</a></li>
        <li><a title="NURS 444 - Nursing Science 2" href="https://hsmoodle.otago.ac.nz/course/view.php?id=423">NURS444 - Nursing Science 2</a></li>
        <li><a title="NURS 445 - Introduction to Clinical Nursing Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=424">NURS 445 - Introduction to Clinical Nursing Practice</a></li>
        <li><a title="NURS 446 - Altered Health States" href="https://hsmoodle.otago.ac.nz/course/view.php?id=425">NURS446 - Altered Health States</a></li>
        <li><a title="NURS 447 - Applied Nursing Research" href="https://hsmoodle.otago.ac.nz/course/view.php?id=426">NURS447 - Applied Nursing Research</a></li>
        <li><a title="Hauora Māori Yr1" href="https://hsmoodle.otago.ac.nz/course/view.php?id=462">Hauora Māori Yr1</a></li>
      </ul>
      <ul class="unstyled span3 mylist">
        <!-- 2018 Master of Nursing Science Degree -->
        
        <li class="programme">2018 Master of Nursing Science Degree Yr1</li>
        <li><a title="Master of Nursing Science 2018" href="https://hsmoodle.otago.ac.nz/course/view.php?id=527">Master of Nursing Science 2018</a></li>
        <li><a title="Nursing Science Pre-Course" href="https://hsmoodle.otago.ac.nz/course/view.php?id=528">Nursing Science Pre-Course</a></li>
        <li><a title="NURS 441 - Professional Nursing" href="https://hsmoodle.otago.ac.nz/course/view.php?id=530">NURS441 - Professional Nursing</a></li>
        <li><a title="NURS 442 - Health Care in NZ" href="https://hsmoodle.otago.ac.nz/course/view.php?id=531">NURS442 - Health Care in NZ</a></li>
        <li><a title="NURS 443 - Nursing Science 1" href="https://hsmoodle.otago.ac.nz/course/view.php?id=532">NURS443 - Nursing Science 1</a></li>
        <li><a title="NURS 444 - Nursing Science 2" href="https://hsmoodle.otago.ac.nz/course/view.php?id=533">NURS444 - Nursing Science 2</a></li>
        <li><a title="NURS 445 - Introduction to Clinical Nursing Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=534">NURS 445 - Introduction to Clinical Nursing Practice</a></li>
        <li><a title="NURS 446 - Altered Health States" href="https://hsmoodle.otago.ac.nz/course/view.php?id=535">NURS446 - Altered Health States</a></li>
        <li><a title="NURS 447 - Applied Nursing Research" href="https://hsmoodle.otago.ac.nz/course/view.php?id=536">NURS447 - Applied Nursing Research</a></li>
        <li><a title="Hauora Māori Yr1" href="https://hsmoodle.otago.ac.nz/course/view.php?id=539">Hauora Māori Yr1</a></li>    
        
        <li><br /></li>

        <li class="programme">2018 Master of Nursing Science Degree Yr2</li>
        <li><a title="NURS 501 - Nursing Science 3" href="https://hsmoodle.otago.ac.nz/course/view.php?id=522">NURS501 - Nursing Science 3</a></li>
        <li><a title="NURS 502 - Mental Health Nursing" href="https://hsmoodle.otago.ac.nz/course/view.php?id=524">NURS502 - Mental Health Nursing</a></li>
        <li><a title="NURS 503 - Complex Health States" href="https://hsmoodle.otago.ac.nz/course/view.php?id=523">NURS503 - Complex Health States</a></li>
        <li><a title="NURS 504 - Consolidation of Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=525">NURS504 - Consolidation of Practice</a></li>
        <li><a title="NURS 580 - Nursing Research Project" href="https://hsmoodle.otago.ac.nz/course/view.php?id=538">NURS580 - Nursing Research Project</a></li>
        <li><a title="NURS 581 - Integrating Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=526">NURS581 - Integrating Practice</a></li>
      </ul>
    </div>
      </li>
    </ul>  
  </div>

  <!-- ********************************************************************************
       *     PUBH Menu 
       ********************************************************************************* --> 
  <div id="pubh" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
    <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">Public Health Navigation<h2></li>
            <li class="home"><a title="Public Health Home" href="https://hsmoodle.otago.ac.nz/course/view.php?id=44">Public Health Home</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#pubh">Close X</button>
        </li>
        
            <li class="home_divider"></li>
          </ul>
    </div>
    <div class="row-fluid">  
          <ul class="unstyled span3 mylist">
        
            <!-- Public Health Term 1 -->        
            <li class="programme"><div>Term 1</div></li>
            <li><a title="PUBH712 Foundations of Hauora Maori" href="https://hsmoodle.otago.ac.nz/course/view.php?id=540">PUBH712 - Foundations of Hauora Maori</a></li>
            <li><a title="PUBH723 Survey Methods" href="https://hsmoodle.otago.ac.nz/course/view.php?id=543">PUBH723 - Survey Methods</a></li>
            <li><a title="PUBH725 Applied Biostatistics 1 - Fundamentals" href="https://hsmoodle.otago.ac.nz/course/view.php?id=541">PUBH725 - Applied Biostatistics 1 - Fundamentals</a></li>
            <li><a title="PUBH733 Environment and Health" href="https://hsmoodle.otago.ac.nz/course/view.php?id=547">PUBH733 - Environment and Health</a></li>
            <li><a title="PUBH735 The Economics of Health Policy Decision Making" href="https://hsmoodle.otago.ac.nz/course/view.php?id=546">PUBH735 - The Economics of Health Policy Decision Making</a></li>
            <li><a title="PUBH737 Public Health Law and Ethics - Fundamentals" href="https://hsmoodle.otago.ac.nz/course/view.php?id=548">PUBH737 - Public Health Law and Ethics - Fundamentals</a></li>
          </ul> 
          <ul class="unstyled span3 mylist">
            <!-- Public Health Term 2 --> 
            <li class="programme">Term 2</li>   
            <li><a title="PUBH711 Principles of Epidemiology" href="https://hsmoodle.otago.ac.nz/course/view.php?id=549">PUBH711 - Principles of Epidemiology</a></li>
            <li><a title="PUBH724 Introduction to Qualitative Research Methods" href="https://hsmoodle.otago.ac.nz/course/view.php?id=551">PUBH724 - Introduction to Qualitative Research Methods</a></li>
            <li><a title="PUBH726 Applied Biostatistics 2 - Regression Methods" href="https://hsmoodle.otago.ac.nz/course/view.php?id=559">PUBH726 - Applied Biostatistics 2 - Regression Methods</a></li>
            <li><a title="PUBH734: Health Protection" href="https://hsmoodle.otago.ac.nz/course/view.php?id=561">PUBH734 - Health Protection</a></li>
            <li><a title="PUBH738 Global Health Law and Ethics" href="https://hsmoodle.otago.ac.nz/course/view.php?id=557">PUBH738 - Global Health Law and Ethics</a></li>
            <li><a title="PUBH741 Hauora Māori - Policy, Practice and Research" href="https://hsmoodle.otago.ac.nz/course/view.php?id=556">PUBH741 - Hauora Māori - Policy, Practice and Research</a></li>
          </ul>
          <ul class="unstyled span3 mylist">
            <!-- Public Health Term 3 -->
            <li class="programme">Term 3</li>
            <li><a title="PUBH713 Society, Health and Health Promotion" href="https://hsmoodle.otago.ac.nz/course/view.php?id=558">PUBH713 - Society, Health and Health Promotion</a></li>
            <li><a title="PUBH732 Prevention and Control of Disease in Populations" href="https://hsmoodle.otago.ac.nz/course/view.php?id=560">PUBH732 - Prevention and Control of Disease in Populations</a></li>
            <li><a title="PUBH742 International Health Systems" href="https://hsmoodle.otago.ac.nz/course/view.php?id=562">PUBH742 - International Health Systems</a></li>
            <li><a title="PUBH744 Healthy Public Policy" href="https://hsmoodle.otago.ac.nz/course/view.php?id=563">PUBH744 - Healthy Public Policy</a></li>
      </ul>
          <ul class="unstyled span3 mylist">
            <!-- Public Health Term 4 -->
            
            <li class="programme">Term 4</li>
            <li><a title="PUBH714 Public Policy and Health Systems" href="https://hsmoodle.otago.ac.nz/course/view.php?id=571">PUBH714 - Public Policy and Health Systems</a></li>
            <li><a title="PUBH721 Methods for Epidemiological Research" href="https://hsmoodle.otago.ac.nz/course/view.php?id=572">PUBH721 - Methods for Epidemiological Research</a></li>
            <li><a title="PUBH736 Economic Evaluation" href="https://hsmoodle.otago.ac.nz/course/view.php?id=573">PUBH736 - Economic Evaluation</a></li>
            <li><a title="PUBH739 Special Topic - Systematic Reviews" href="https://hsmoodle.otago.ac.nz/course/view.php?id=574">PUBH739 - Special Topic - Systematic Reviews</a></li>
            <li><a title="PUBH743 Health Promotion Programme Planning and Evaluation" href="https://hsmoodle.otago.ac.nz/course/view.php?id=575">PUBH743 - Health Promotion Programme Planning and Evaluation</a></li>

      </ul>
        </div>
      </li>
    </ul>  
  </div>


  <!-- ********************************************************************************
       *     RADT Menu 
       ********************************************************************************* --> 
  <div id="radt" class="collapse brnav-block">
    <!-- RADT Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
        <div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">Radiation Therapy<h2></li>
            <li class="home"><a title="RADT - Home" href="https://hsmoodle.otago.ac.nz/course/view.php?id=275">Radiation Therapy - Home</a><button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#radt">Close X</button></li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">
          <ul class="unstyled span6 mylist">
            <li class="programme">Undergraduate</li>
            <li><a title="Year 1" href="https://hsmoodle.otago.ac.nz/course/view.php?id=347">Year 1</a></li>
            <li><a title="Year 2" href="https://hsmoodle.otago.ac.nz/course/view.php?id=348">Year 2</a></li>
            <li><a title="Year 3" href="https://hsmoodle.otago.ac.nz/course/view.php?id=349">Year 3</a></li>
            <li><a title="Clinical Department Information" href="https://hsmoodle.otago.ac.nz/course/view.php?id=346">Clinical Department Information</a></li>
            <li><a title="Clinical Records" href="https://hsmoodle.otago.ac.nz/course/view.php?id=350">Clinical Records (Staff only)</a></li>
          </ul>
          <ul class="unstyled span6 mylist">         
            <li class="programme">Postgraduate</li>
            <li><a title="RADT PostGrad Hub" href="https://hsmoodle.otago.ac.nz/course/view.php?id=351">Postgraduate Hub</a></li>
            <li><a title="RADT 401" href="https://hsmoodle.otago.ac.nz/course/view.php?id=477">RADT401 - Applied Radiation Therapy Advanced Practice</a></li>
            <li><a title="RADT 402" href="https://hsmoodle.otago.ac.nz/course/view.php?id=478">RADT402 - Patient Centred Radiation Therapy Advanced Practice</a></li>
            <li><a title="RADT 403" href="https://hsmoodle.otago.ac.nz/course/view.php?id=479">RADT403 - Brachytherapy Principles and Practice</a></li>            
          </ul>
        </div>
      </li>
    </ul>  
  </div>
  
  
  <!-- ********************************************************************************
       *     RTRU Menu 
       ********************************************************************************* --> 
  <div id="rtru" class="collapse brnav-block">
    
    <!-- RTRU Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
    <div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">RTRU Navigation<h2></li>
            <li class="home">&nbsp;
          <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#rtru">Close X</button>
        </li>
            <li class="home_divider"></li>
          </ul>
    </div>
    <div class="row-fluid">
          <ul class="unstyled span6 mylist">
            
            <!--RTRU-->
            <li class="school-title">Courses</li>
            <li><a title="REHB701" href="https://hsmoodle.otago.ac.nz/course/view.php?id=353">REHB701 - Rehabilitation Principles</a></li>
            <li><a title="REHB703" href="https://hsmoodle.otago.ac.nz/course/view.php?id=354">REHB703 - Musculoskeletal Rehabilitation</a></li>
            <li><a title="REHB704" href="https://hsmoodle.otago.ac.nz/course/view.php?id=355">REHB704 - Neurological Rehabilitation</a></li>
            <li><a title="REHB706" href="https://hsmoodle.otago.ac.nz/course/view.php?id=356">REHB706 - Work Rehabilitation</a></li>
            <li><a title="REHB707" href="https://hsmoodle.otago.ac.nz/course/view.php?id=357">REHB707 - Rehabilitation for Older Adults</a></li>
            <li><a title="REHB709" href="https://hsmoodle.otago.ac.nz/course/view.php?id=358">REHB709 - Family Systems</a></li> 
            <li><a title="REHB710" href="https://hsmoodle.otago.ac.nz/course/view.php?id=359">REHB710 - Clinical Rehabilitation</a></li>
          </ul>
          <ul class="unstyled span6 mylist">
            <li class="school-title">&nbsp;</li> 
            <li><a title="REHB712" href="https://hsmoodle.otago.ac.nz/course/view.php?id=360">REHB712 - Evaluating Rehabilitation: Challenges and Opportunities</a></li> 
            <li><a title="REHB713" href="https://hsmoodle.otago.ac.nz/course/view.php?id=361">REHB713 - Goal Setting and the Therapeutic Relationship</a></li> 
            <li><a title="REHB714" href="https://hsmoodle.otago.ac.nz/course/view.php?id=362">REHB714 - Personal and Psychological Factors in Rehabilitation</a></li> 
            <li><a title="REHB715" href="https://hsmoodle.otago.ac.nz/course/view.php?id=363">REHB715 - Cardiac and Pulmonary Rehabilitation</a></li> 
            <li><a title="REHB716" href="https://hsmoodle.otago.ac.nz/course/view.php?id=364">REHB716 - Rehabilitation with Children</a></li> 
            <li><a title="RTRU Masters" href="https://hsmoodle.otago.ac.nz/course/view.php?id=35">RTRU Masters</a></li> 
            <li><a title="HASC701" href="https://hsmoodle.otago.ac.nz/course/view.php?id=352">HASC701 - Working in Interprofessional Clinical Teams</a></li>
      </ul>
    </div>
      </li>
    </ul>
  </div>

  <!-- ********************************************************************************
       *     GENA Menu 
       ********************************************************************************* --> 
  <div id="gena" class="collapse brnav-block">
    
    <!-- GENA Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
    <div class="row-fluid">
      <ul class="unstyled span12">
        <li><h2 class="brnav-heading">GP & Rural Health Navigation</h2></li>
        <li class="home">&nbsp;
          <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#gena">Close X</button>
        </li>
        <li class="home_divider"></li>
      </ul>
    </div>
    <div class="row-fluid">
      <ul class="unstyled span6 mylist">
        <!--GENA-->
        <li class="school-title">Courses</li>
        <li><a title="Generalist Medical Echocardiography & Ultrasound - GENA 717 & 718" href="https://hsmoodle.otago.ac.nz/course/view.php?id=366">GENA717 & 718 - Generalist Medical Echocardiography & Ultrasound</a></li>
        <li><a title="Trauma and Emergencies in Rural Hospitals - GENA 723" href="https://hsmoodle.otago.ac.nz/course/view.php?id=367">GENA723 - Trauma and Emergencies in Rural Hospitals</a></li>
        <li><a title="The Context of Rural Hospital Medicine - GENA 724" href="https://hsmoodle.otago.ac.nz/course/view.php?id=310">GENA724 - The Context of Rural Hospital Medicine</a></li>
        <li><a title="Communication in Rural Hospital Medicine GENA725" href="https://hsmoodle.otago.ac.nz/course/view.php?id=368">GENA725 - Communication in Rural Hospital Medicine</a></li>
        <li><a title="Paediatrics and O&G - GENA 726" href="https://hsmoodle.otago.ac.nz/course/view.php?id=309">GENA726 - Paediatrics and O&G</a></li>
        <li><a title="Surgical Specialties in Rural Hospitals GENA727" href="https://hsmoodle.otago.ac.nz/course/view.php?id=369">GENA727 - Surgical Specialties in Rural Hospitals</a></li>
      </ul>
      <ul class="unstyled span6 mylist">
        <li class="school-title">&nbsp;</li>
        <li><a title="Cardiorespiratory Medicine in Rural Hospitals - GENA 728" href="https://hsmoodle.otago.ac.nz/course/view.php?id=370">GENA728 - Cardiorespiratory Medicine in Rural Hospitals</a></li>
        <li><a title="Medical Specialties in Rural Hospitals - GENA 729" href="https://hsmoodle.otago.ac.nz/course/view.php?id=460">GENA729 - Medical Specialties in Rural Hospitals</a></li>
        <li><a title="Research Methods in General Practice GENA821" href="https://hsmoodle.otago.ac.nz/course/view.php?id=459">GENA821 - Research Methods in General Practice</a></li>
        <li><a title="Culture Health and Society GENA 825" href="https://hsmoodle.otago.ac.nz/course/view.php?id=435">GENA825 - Culture Health and Society</a></li>
        <li><a title="Teaching and Learning in Medical Practice GENA 823" href="https://hsmoodle.otago.ac.nz/course/view.php?id=461">GENA823 - Teaching and Learning in Medical Practice</a></li>
      </ul>
    </div>
      </li>
    </ul>
  </div>


  <!-- ********************************************************************************
       *     PSYC Menu 
       ********************************************************************************* -->  
  <div id="psyc" class="collapse brnav-block">
    
    <!-- PYSC Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
        <div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">Psychological Medicine Navigation</h2></li>
            <li class="home">&nbsp;
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#psyc">Close X</button>
            </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">
          <ul class="unstyled span6 mylist">
            <!--PSYC-->
            <li class="programme">Semester 1</li>
            <li><a title="PSME401 - Nature Extent & Assessment of Mental Disorders" href="https://hsmoodle.otago.ac.nz/course/view.php?id=587">PSME401 - Nature Extent & Assessment of Mental Disorders</a></li>
            <li><a title="NURS403 - Mental Health Nursing Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=579">NURS403 - Mental Health Nursing Practice</a></li>
            <li><a title="PSME432 - Introduction to Perinatal Psychiatry" href="https://hsmoodle.otago.ac.nz/course/view.php?id=581">PSME432 - Introduction to Perinatal Psychiatry</a></li>
            <li><a title="PSME433 - Introduction to Infant Mental Health" href="https://hsmoodle.otago.ac.nz/course/view.php?id=586">PSME433 - Introduction to Infant Mental Health</a></li>              
            <li><a title="PSME436 - Principles of Family and Systems Theory" href="https://hsmoodle.otago.ac.nz/course/view.php?id=580">PSME436 - Principles of Family and Systems Theory</a></li>
            <li><a title="PSME437 - Applied Systems Theory" href="https://hsmoodle.otago.ac.nz/course/view.php?id=585">PSME437 - Applied Systems Theory</a></li>
          </ul>
          <ul class="unstyled span6 mylist">         
            <li class="programme">Semester 2</li>
            <li><a title="NURS404 - Advanced Mental Health Nursing Practice" href="https://hsmoodle.otago.ac.nz/course/view.php?id=584">NURS404 - Advanced Mental Health Nursing Practice</a></li>
            <li><a title="PSME405 - Contemporary Approaches to Mental Health" href="https://hsmoodle.otago.ac.nz/course/view.php?id=578">PSME405 - Contemporary Approaches to Mental Health</a></li>
            <li><a title="PSME406 - Research Methods: Mental Health" href="https://hsmoodle.otago.ac.nz/course/view.php?id=582">PSME406 - Research Methods: Mental Health</a></li>
            <li><a title="NURS409 - Mental Health Nursing Practicum" href="https://hsmoodle.otago.ac.nz/course/view.php?id=583">NURS409 - Mental Health Nursing Practicum</a></li>
          </ul>
        </div>
      </li>
    </ul>  
  </div>

  <!-- ********************************************************************************
       *     AvMed Menu 
       ********************************************************************************* --> 
  <div id="avmed" class="collapse brnav-block">
    
    <!-- AvMed Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
    <div class="row-fluid">
      <ul class="unstyled span12">
        <li><h2 class="brnav-heading">Occupational and Aviation Medicine Navigation</h2></li>
        <li class="home"><a title="Faculty Area" href="https://hsmoodle.otago.ac.nz/course/view.php?id=456">Faculty Area</a>
          <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#avmed">Close X</button>
        </li>
        <li class="home_divider"></li>
      </ul>
    </div>
    <div class="row-fluid">
      
      <ul class="unstyled span4 mylist">        
            <li class="programme">Occupational Medicine</li>
            <li><a title="AVME 715" href="https://hsmoodle.otago.ac.nz/course/view.php?id=441">AVME715 - Health, Work and Behaviour</a></li>
            <li><a title="AVME 716" href="https://hsmoodle.otago.ac.nz/course/view.php?id=442">AVME716 - Clinical Occupational Medicine</a></li>
            <li><a title="AVME 801" href="https://hsmoodle.otago.ac.nz/course/view.php?id=443">AVME801 - Occupational Medicine, Epidemiology and Biostatistics</a></li>
            <li><a title="AVME 802" href="https://hsmoodle.otago.ac.nz/course/view.php?id=444">AVME802 - Vocational Rehabilitation</a></li>
      </ul>
      <ul class="unstyled span4 mylist">        
        <li class="programme">Aviation Medicine</li>
        <li><a title="AVME 712" href="https://hsmoodle.otago.ac.nz/course/view.php?id=438">AVME712 - Aircrew Health and Performance</a></li>
        <li><a title="AVME 713" href="https://hsmoodle.otago.ac.nz/course/view.php?id=437">AVME713 - Airport and Travel Health</a></li>
      </ul>
      <ul class="unstyled span4 mylist">        
            <li class="programme">Aeromedical Retrieval and Transport</li>
            <li><a title="AVME 717" href="https://hsmoodle.otago.ac.nz/course/view.php?id=449">AVME717 - Medical Logistics in Aeromedical Transport</a></li>
            <li><a title="AVME 718" href="https://hsmoodle.otago.ac.nz/course/view.php?id=450">AVME718 - Operational Aspects of Aeromedical Transport</a></li>
            <li><a title="AVME 719" href="https://hsmoodle.otago.ac.nz/course/view.php?id=447">AVME719 - Operational Aspects of Aeromedical Retrieval</a></li>
            <li><a title="AVME 720" href="https://hsmoodle.otago.ac.nz/course/view.php?id=448">AVME720 - Clinical Aspects of Aeromedical Retrieval</a></li>
      </ul>
    </div>
      </li>
    </ul>
  </div>

  <!-- ********************************************************************************
       *     SPME Menu 
       ********************************************************************************* --> 
  <div id="spme" class="collapse brnav-block">
    
    <!-- SPME Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
    <div class="row-fluid">
      <ul class="unstyled span12">
        <li><h2 class="brnav-heading">Sports Medicine Navigation<h2></li>
        <li class="home">&nbsp;
          <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#spme">Close X</button>
        </li>
        <li class="home_divider"></li>
      </ul>
    </div>
    <div class="row-fluid">
      <ul class="unstyled span4 mylist">
        
        <li class="programme">Sports Medicine</li>
        <li><a title="SPME 701" href="https://hsmoodle.otago.ac.nz/course/view.php?id=468">SPME701 - Issues in Sports Medicine</a></li>
        <li><a title="SPME 702" href="https://hsmoodle.otago.ac.nz/course/view.php?id=465">SPME702 - Medical Aspects of Exercise</a></li>
        <li><a title="SPME 703" href="https://hsmoodle.otago.ac.nz/course/view.php?id=470">SPME703 - Sports Nutrition</a></li>
        <li><a title="SPME 705" href="https://hsmoodle.otago.ac.nz/course/view.php?id=466">SPME705 - Health and Human Performance</a></li>
        <li><a title="SPME 707" href="https://hsmoodle.otago.ac.nz/course/view.php?id=467">SPME707 - Regional Sports Injury 1 </a></li>
        <li><a title="SPME 708" href="https://hsmoodle.otago.ac.nz/course/view.php?id=471">SPME708 - Regional Sports Injury 2</a></li>
        <li><a title="SPME 709" href="https://hsmoodle.otago.ac.nz/course/view.php?id=469">SPME709 - Women in Sport: Health Issues</a></li>
        <li><a title="SPME 711" href="https://hsmoodle.otago.ac.nz/course/view.php?id=314">SPME711 - Exercise Prescription</a></li>
      </ul>
    </div>
      </li>
    </ul>
  </div>
  <!-- ********************************************************************************
       *     OTHER COURSES Menu 
       ********************************************************************************* --> 
  <div id="other" class="collapse brnav-block">
    
    <!-- Other Courses Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
    <div class="row-fluid">
      <ul class="unstyled span12">
        <li><h2 class="brnav-heading">Other Courses Navigation</h2></li>
        <li class="home">&nbsp;
          <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#other">Close X</button>
        </li>
        <li class="home_divider"></li>
      </ul>
    </div>
    <div class="row-fluid">
      <ul class="unstyled span6 mylist">
        <!--OTHER-->
        <li class="school-title">Courses</li>
        
        <li><a title="CTMG401 - Pathophysiology of Incontinence" href="https://hsmoodle.otago.ac.nz/course/view.php?id=321">CTMG401 - Pathophysiology of Incontinence</a></li>
            <li><a title="CTMG402 - Management for Continence" href="https://hsmoodle.otago.ac.nz/course/view.php?id=322">CTMG402 - Management for Continence</a></li>
            <li><a title="1 CHHE702 Clinical Attachment in Paediatrics" href="https://hsmoodle.otago.ac.nz/course/view.php?id=319">CHHE702 - Sem 1 Clinical Attachment in Paediatrics</a></li>
            <li><a title="2 CHHE702 Clinical Attachment in Paediatrics" href="https://hsmoodle.otago.ac.nz/course/view.php?id=320">CHHE702 - Sem 2 Clinical Attachment in Paediatrics</a></li>
            <li><a title="COBE401 Cognitive Behaviour Therapy" href="https://hsmoodle.otago.ac.nz/course/view.php?id=318">COBE401 - Cognitive Behaviour Therapy</a></li>
          </ul>
          <ul class="unstyled span6 mylist">
            <li class="school-title">&nbsp;</li>
            <li><a title="Bachelor of Biomedical Sciences with Honours" href="https://hsmoodle.otago.ac.nz/course/view.php?id=315">Bachelor of Biomedical Sciences with Honours</a></li>
        <li><a title="Basic Physician Training" href="https://hsmoodle.otago.ac.nz/course/view.php?id=110">Basic Physician Training</a></li>
        <li><a title="PsychMed Registrars Resources" href="https://hsmoodle.otago.ac.nz/course/view.php?id=474">Resources for Psychological Medicine Registrars</a></li>
      </ul>
    </div>
      </li>
    </ul>
  </div>

  <!-- ********************************************************************************
       *     HELP Menu 
       ********************************************************************************* -->  
  <div id="helpmenu" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
    <div class="row-fluid" >
      <ul class="unstyled span12">
        <li><h2 class="brnav-heading">Help Navigation</h2></li>
        <li class="home"><a title="Contact Support- email med.moodle@otago.ac.nz" href="mailto:med.moodle@otago.ac.nz?Subject=Help with Moodle&body=In order to help you, we need details of your browser and operating system. %0D%0DPLEASE DO THE FOLLOWING%0D%0D1. go to http://www.otago.ac.nz/checkinfo %0D%0D2. COPY the information from the first white box. %0D%0D3. paste the results into this email. %0D%0DNow please describe the issue you are experiencing:">Contact Support- email med.moodle@otago.ac.nz</a>
          <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#helpmenu">Close X</button>
        </li>
            <li class="home_divider"></li>
          </ul>
    </div>
    <div class="row-fluid">  
          <ul class="unstyled span6 mylist">
            <li class="programme">Student Support</li>
            <li><a title="Moodle Student Support" href="https://hsmoodle.otago.ac.nz/course/view.php?id=114">Moodle Student Support</a></li>
            <li><a title="Embark Otago Orientation" href="https://hsmoodle.otago.ac.nz/course/view.php?id=454">Embark Otago - Orientation</a></li>
          </ul> 
          <ul class="unstyled span6 mylist">
            <li class="programme">Staff Support</li> 
            <li><a title="Moodle Staff Support" href="https://hsmoodle.otago.ac.nz/course/view.php?id=115">Moodle Staff Support</a></li>
            <li><a title="Video Tutorials for staff" href="https://hsmoodle.otago.ac.nz/course/view.php?id=115&section=2">Video Tutorials for staff</a></li>
            <li><a title="Moodle Handbook" href="http://facmed.otago.ac.nz/elearning/wp-content/uploads/sites/6/2010/12/using_moodle_2e.pdf" target="_blank">Moodle Handbook for staff</a></li>
          </ul>
    </div>
      </li>
    </ul>  
  </div>
