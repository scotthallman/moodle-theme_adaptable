// medadaptable.js -- js initialisations for Otago Medical School version
// of Adaptable theme for Moodle.

define(['jquery',
	'theme_adaptable/jquery.expander',
	'theme_adaptable/jquery.flexslider',
	'theme_bootstrapbase/bootstrap'], function($) {

  var init_expander = function() {
    $(document).ready(function() {
      var opts = {collapseTimer: 4000};

      // $.each(['beforeExpand', 'afterExpand', 'onCollapse'], function(i, callback) {
      // opts[callback] = function(byUser) {
      // 	var by, msg = '<div class="success">' + callback;
      
      // 	if (callback == 'onCollapse') {
      //         msg += ' (' + (byUser ? 'user' : 'timer') + ')';
      // 	}
      // 	msg += '</div>';
      
      // 	$(this).parent().parent().append(msg)
      // }
      // });
      
      $('dl.expander dd').eq(0).expander();
      $('dl.expander dd').slice(1).expander(opts);
      
      
      $('ul.expander li').expander({
	slicePoint: 50,
	widow: 2,
	expandSpeed: 0,
	userCollapseText: '[^]'
      });
      
      $('div.expander').expander();
      
      
      //    $('oms-popover').popover({'trigger': 'click'});
    });
  };

  var init_popover = function() {
    $('[data-toggle="popover"]').popover({'trigger': 'click'});
  };

  var init_flexslider = function() {
    $('#main-slider').flexslider({
        namespace           : "flex-",
        selector            : ".slides > li",
        animation           : "slide",
        easing              : "swing",
        direction           : "horizontal",
        reverse             : false,
        animationLoop       : true,
        smoothHeight        : false,
        startAt             : 0,
        slideshow           : true,
        slideshowSpeed      : 20000,
        animationSpeed      : 600,
        initDelay           : 0,
        randomize           : false,
        pauseOnAction       : true,
        pauseOnHover        : false,
        useCSS              : true,
        touch               : true,
        video               : false,
        controlNav          : true,
        directionNav        : true,
        prevText            : "Previous",
        nextText            : "Next",
        keyboard            : true,
        multipleKeyboard    : false,
        mousewheel          : false,
        pausePlay           : false,
        pauseText           : 'Pause',
        playText            : 'Play',
        controlsContainer   : "",
        manualControls      : "",
        sync                : "",
        asNavFor            : "",
    });
    $('div.flexslider.slider-std').flexslider({
        namespace           : "flex-",
        selector            : ".slides > li",
        animation           : "slide",
        easing              : "swing",
        direction           : "horizontal",
        // reverse             : false,
        // animationLoop       : true,
        // smoothHeight        : false,
        // startAt             : 0,
        // slideshow           : true,
        // slideshowSpeed      : 20000,
        // animationSpeed      : 600,
        // initDelay           : 0,
        // randomize           : false,
        // pauseOnAction       : true,
        // pauseOnHover        : false,
        // useCSS              : true,
        // touch               : true,
        // video               : false,
        // controlNav          : true,
        // directionNav        : true,
        prevText            : "",
        nextText            : "",
        // keyboard            : true,
        // multipleKeyboard    : false,
        // mousewheel          : false,
        // pausePlay           : false,
        // pauseText           : 'Pause',
        // playText            : 'Play',
        controlsContainer   : $(this).parent(),
        // manualControls      : "",
        // sync                : "",
        // asNavFor            : "",
    });
    $('div.flexslider.slider-fade').flexslider({
        namespace           : "flex-",
        selector            : ".slides > li",
        animation           : "fade",
        easing              : "swing",
        direction           : "horizontal",
        reverse             : false,
        animationLoop       : true,
        smoothHeight        : false,
        startAt             : 0,
        slideshow           : true,
        slideshowSpeed      : 20000,
        animationSpeed      : 600,
        initDelay           : 0,
        randomize           : false,
        pauseOnAction       : true,
        pauseOnHover        : false,
        useCSS              : true,
        touch               : true,
        video               : false,
        controlNav          : true,
        directionNav        : true,
        prevText            : "Previous",
        nextText            : "Next",
        keyboard            : true,
        multipleKeyboard    : false,
        mousewheel          : false,
        pausePlay           : false,
        pauseText           : 'Pause',
        playText            : 'Play',
        controlsContainer   : "",
        manualControls      : "",
        sync                : "",
        asNavFor            : "",
    });

    $(".container.slidewrap").on('transitionend', function() {
      // trigger resize events on flexslider divs when container (not present in our
      // current uses, but used on #main-slider) ends a transition.
      // Not sure what purpose is - maybe working around apparent ugliness with
      // CSS3 transitions?
      var sliders;
      //sliders = $('div.flexslider.slider-std').data('flexslider');
      //sliders.resize();
      //sliders = $('div.flexslider.slider-fade').data('flexslider');
      //sliders.resize();
      sliders = $('#main-slider').data('flexslider');
      sliders.resize();
    })
  };

  return {
    oms_init: function () {
      init_expander();
      init_popover();
      init_flexslider();
    }
  };
});
